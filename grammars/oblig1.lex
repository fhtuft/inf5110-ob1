package oblig1parser;
import java_cup.runtime.*;
%%

%class Lexer
%unicode
//%debug
%cup
%line
%column
%public
%{
 StringBuffer string = new StringBuffer();

  private Symbol symbol(int type) {
    return new Symbol(type, yyline, yycolumn);
  }
  private Symbol symbol(int type, Object value) {
    return new Symbol(type, yyline, yycolumn, value);
  }

%}
InputCharacter = [^\r\n]
LineTerminator = \r|\n|\r\n
WhiteSpace = {LineTerminator} | [ \t\f]
Nat = [0-9]+
Identifier = [:jletter:] [:jletterdigit:]*
Comment = "//"{InputCharacter}*{LineTerminator}?
IntLiteral = {Nat} 
FloatLiteral = {Nat}.{Nat} 

%state STRING

 
%%
<YYINITIAL>{
        
        /* Whitespace */
        {WhiteSpace}                    { /* whiteSpace */ } //0
        {Comment}                       { /* Comment */ } //1
        
    
        /* Operators */
        "+"								{ return symbol(sym.PLUS); } //2
	    "-"								{ return symbol(sym.MINUS); } //3
        "*"								{ return symbol(sym.MULTI); } //4
	    "/"								{ return symbol(sym.DIV); } //5
	    "#"								{ return symbol(sym.EXPON); } //6
	    "&&"							{ return symbol(sym.LOGIC_AND); } //7
	    "||"							{ return symbol(sym.LOGIC_ELSE); }//8
	    "<"								{ return symbol(sym.LESS); }//9
	    "<="							{ return symbol(sym.LESS_EQUAL); }//10
	    ">"								{ return symbol(sym.GREATER); }//11
	    ">="							{ return symbol(sym.GREATER_EQUAL); }//12
	    "="								{ return symbol(sym.EQUAL); }//13
	    "<>"							{ return symbol(sym.LESS_GREATER); }//14
	

        /* Stuff */    
	    "."								{ return symbol(sym.DOT); }//15
        ":"							    { return symbol(sym.COLON); }//16
        ":="							{ return symbol(sym.COLON_EQUAL); }//17
        ","                             { return symbol(sym.COMMA); }//18
        "("                             { return symbol(sym.LPAR); }//19
        ")"                             { return symbol(sym.RPAR); }//20
        ";"                             { return symbol(sym.SEMI); }//21

        /* Builtins */
        "begin"                         { return symbol(sym.BEGIN); }//22
        "bool"							{ return symbol(sym.BOOL); }//23
	    "class"                         { return symbol(sym.CLASS); }//24
	    "do"							{ return symbol(sym.DO); }//25
	    "else"							{ return symbol(sym.ELSE); }//26
        "end"                           { return symbol(sym.END); }//27
	    "float"                         { return symbol(sym.FLOAT); }//28
	    "if"							{ return symbol(sym.IF); }//29
	    "int"							{ return symbol(sym.INT); }//30
	    "not"							{ return symbol(sym.NOT); }//31
        "new"                           { return symbol(sym.NEW); }//32
	    "proc"							{ return symbol(sym.PROC); }//33
	    "program"                       { return symbol(sym.PROGRAM); }//34
	    "ref"							{ return symbol(sym.REF); }//35
	    "return"						{ return symbol(sym.RETURN); }//36
	    "string"						{ return symbol(sym.STRING); }//37
	    "then"							{ return symbol(sym.THEN); }//38
	    "var"							{ return symbol(sym.VAR); }//39
	    "while"							{ return symbol(sym.WHILE); }//40
    
      

        /* Literals */
	    "false"	                        { return symbol(sym.FALSE); }//41
	    "true"							{ return symbol(sym.TRUE); }//42
	    "null"							{ return symbol(sym.NULL); }//43
        {IntLiteral}					{ return symbol(sym.INT_LITERAL, yytext()); }//44
	    {FloatLiteral}					{ return symbol(sym.FLOAT_LITERAL, yytext()); }//45
	   
         
        /* Identifier */
        {Identifier}                    { return symbol(sym.ID,yytext()); }//46

 
        
        /* String, change state to in string*/
        \"								{ string.setLength(0); yybegin(STRING); }


}
//from the manual
<STRING> {
      \"                             { yybegin(YYINITIAL); 
                                       return symbol(sym.STRING_LITERAL, 
                                       string.toString()); }
      [^\n\r\"\\]+                   { string.append( yytext() ); }
      \\t                            { string.append('\t'); }
      \\n                            { string.append('\n'); }

      \\r                            { string.append('\r'); }
      \\\"                           { string.append('\"'); }
      \\                             { string.append('\\'); }
    }
.                           { throw new Error("Illegal character '" + yytext() + "' at line " + yyline + ", column " + yycolumn + "."); }
