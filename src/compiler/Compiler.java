package compiler;

import java.io.*;

import bytecode.CodeFile;

import syntaxtree.*;
import oblig1parser.*;
//import compiler.Symtabl;

public class Compiler {
    private String inFilename;// = null;
    private String astFilename;// = null;
    private String binFilename;// = null;
    
    public String syntaxError;
    public String error;
    public Compiler(String inFilename, String astFilename, String binFilename){
        this.inFilename = inFilename;
        this.astFilename = astFilename;
        this.binFilename = binFilename;
    }
    public int compile() throws Exception {
        InputStream inputStream = new FileInputStream(this.inFilename);
        Lexer lexer = new Lexer(inputStream);
        parser parser = new parser(lexer);
        Symtabl symtabl = new Symtabl();       

        //Add stdlib
        StdLib.insertStdLib(symtabl);
 
        /* Parse */
        Program program;
        try {
            program = (Program)parser.parse().value;
        } catch (SyntaxError e) {
            this.syntaxError = e.getMessage();
            return 1;
		}
            
        /* Check semantics */
        try {
            program.checkSemantic(symtabl);
        } catch(SemanticError e) {
            this.error = e.getMessage();
            return 2;
        }        
        
        /* Write the AST to file */
        writeAST(program);

        /* Generate Code */
        generateCode(program);

        return 0;

    }
    private void writeAST(Program program) throws Exception {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.astFilename));
        bufferedWriter.write(program.printAst());
        bufferedWriter.close();
    }
 
    private void generateCode(Program program) throws Exception {
        CodeFile codeFile = new CodeFile();
        program.generateCode(codeFile);
        byte[] bytecode = codeFile.getBytecode();
        DataOutputStream stream = new DataOutputStream(new FileOutputStream (this.binFilename));
        stream.write(bytecode);
        stream.close();
    }
    public static void main(String[] args) {
        Compiler compiler = new Compiler(args[0], args[1], args[2]);
        int result;
        try {
            result = compiler.compile();
            if(result == 1){
                System.out.println(compiler.syntaxError);
            } else if(result == 2){
                System.out.println(compiler.error);
            }
            System.exit(result);
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
            // If unknown error.
            System.exit(3);
        }
    }
    public static String indent(int indent){
        String result = "";
        for(int i=0;i<indent; i++){
            result+=" ";
        }
        return result;
    }
}
