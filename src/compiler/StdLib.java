package compiler;

import java.util.*;
import syntaxtree.*; //TODO: 

public class StdLib {

    public static void insertStdLib(Symtabl symtabl)  throws SemanticError{
        
        /* We have to fake it for the typechecker */
        LinkedList<Stmt> tmpStmt; //Used to store the fake retur statment
        tmpStmt = new LinkedList<Stmt>();
        tmpStmt.add(new ReturnStmt(new IntLiteral("") )); 
        symtabl.addElement("readint",new ProcDecl("readint",new LinkedList<ParamDecl>(), 
        new DataType(Type.INT), new LinkedList<Decl>(),tmpStmt));

        tmpStmt = new LinkedList<Stmt>();
        tmpStmt.add(new ReturnStmt(new FloatLiteral(""))); 
        symtabl.addElement("readfloat",new ProcDecl("readfloat",new LinkedList<ParamDecl>(), 
        new DataType(Type.FLOAT), new LinkedList<Decl>(),tmpStmt));
        
        tmpStmt = new LinkedList<Stmt>();
        tmpStmt.add(new ReturnStmt(new IntLiteral(""))); 
        symtabl.addElement("readchar",new ProcDecl("readchat",new LinkedList<ParamDecl>(), 
        new DataType(Type.INT), new LinkedList<Decl>(),tmpStmt));
        
        tmpStmt = new LinkedList<Stmt>();
        tmpStmt.add(new ReturnStmt(new StringLiteral(""))); 
        symtabl.addElement("readstring",new ProcDecl("readstring",new LinkedList<ParamDecl>(), 
        new DataType(Type.STRING), new LinkedList<Decl>(),tmpStmt));
        
        tmpStmt = new LinkedList<Stmt>();
        tmpStmt.add(new ReturnStmt(new StringLiteral(""))); 
        symtabl.addElement("readline",new ProcDecl("readline",new LinkedList<ParamDecl>(), 
        new DataType(Type.STRING), new LinkedList<Decl>(),tmpStmt));


        LinkedList<ParamDecl> tmpPDecl;
        tmpPDecl = new LinkedList<ParamDecl>();
        tmpPDecl.add(new ParamDecl("stdlib1",new DataType(Type.INT),false));
        symtabl.addElement("printint",new ProcDecl("printint",tmpPDecl, 
        new DataType(Type.VOID), new LinkedList<Decl>(),new LinkedList<Stmt>() ));

        tmpPDecl = new LinkedList<ParamDecl>();
        tmpPDecl.add(new ParamDecl("stdlib2",new DataType(Type.FLOAT),false));
        symtabl.addElement("printfloat",new ProcDecl("printfloat",tmpPDecl, 
        new DataType(Type.VOID), new LinkedList<Decl>(),new LinkedList<Stmt>()));
        
        
        tmpPDecl = new LinkedList<ParamDecl>();
        tmpPDecl.add(new ParamDecl("stdlib3",new DataType(Type.STRING),false));
        symtabl.addElement("printstr",new ProcDecl("printstr",tmpPDecl, 
        new DataType(Type.VOID), new LinkedList<Decl>(),new LinkedList<Stmt>()));


        tmpPDecl = new LinkedList<ParamDecl>();
        tmpPDecl.add(new ParamDecl("stdlib4",new DataType(Type.STRING),false));
        symtabl.addElement("printline",new ProcDecl("printline",tmpPDecl, 
        new DataType(Type.VOID), new LinkedList<Decl>(),new LinkedList<Stmt>()));
 
 
    }
    

}
