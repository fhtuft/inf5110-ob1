package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.instructions.PUSHBOOL;

public class BoolLiteral extends Literal {
   
    Boolean value;
    public BoolLiteral(Boolean value) {
        this.value = value; 
    }
    public String printAst() {
        return "(BOOL_LITERAL " + value.toString() +")";
    }


    @Override 
    public DataType getDType() {
        return new DataType(Type.BOOL);
    } 

    @Override
    public Type getType() {
        return Type.BOOL;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }


    public void generateCode(CodeProcedure codeProc) {
        codeProc.addInstruction(new PUSHBOOL(this.value));
    }

}
