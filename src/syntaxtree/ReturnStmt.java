package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class ReturnStmt extends Stmt {
    
    Exp e = null;
    
    public ReturnStmt() {
        
    }

    public ReturnStmt(Exp e) {
        this.e = e;
    }

    public String printAst() {
        return "(RETURN_STMT "+ ((e == null) ? "" : e.printAst()) +")\n";
    }

    public Type getReturnType() {
        if(e == null) {
            return Type.VOID;
        }else {
            return e.getType();
        }
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        if(this.e != null)
            this.e.checkSemantic(symtabl);
 }

    @Override
    public void generateCode(CodeProcedure codeProc) {

    }

}

