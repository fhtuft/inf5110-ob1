package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;


public class ArithExp extends Exp {
   
    ArithType arType;
    Exp leftExp,rightExp;
    Type type; 
    DataType dType;

    public ArithExp(ArithType arType, Exp leftExp,Exp rightExp) {
        this.arType = arType;
        this.leftExp = leftExp;
        this.rightExp = rightExp;
    }

    public String printAst() {
        return "ARIT_OP "+ arType.toString() +"\n" + "\t" + leftExp.printAst() + "\n" +"\t" + rightExp.printAst();
    }

    public Type getType() {
        return type; 
    }

    public DataType getDType() {
        return this.dType;
    }


     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
        
        this.leftExp.checkSemantic(symtabl);
        this.rightExp.checkSemantic(symtabl);

        Type leftType = this.leftExp.getType(),rightType = this.rightExp.getType();
 
        /* Check left side */
        if(!(leftType == Type.FLOAT || leftType == Type.INT))  
            throw new SemanticError("ArithExp: Left type  is non legel type " + leftType.toString());
        /* Check right side */
        if(!(rightType == Type.FLOAT || rightType == Type.INT)) 
            throw new SemanticError("ArithExp: Right type is non legal type " + rightType.toString());

        /* Rule: If one operand is Float hole expresion is Float */
        if(leftType == Type.FLOAT || rightType == Type.FLOAT) {
            this.type = Type.FLOAT;
            this.dType = new DataType(Type.FLOAT);
        }else if(arType == ArithType.EXPO) {
        /* Rule: Exponent is always Float */ 
            this.type = Type.FLOAT;
            this.dType = new DataType(Type.FLOAT);
        }    else {
            this.type = leftType;
            this.dType = new DataType(this.type); //TODO: Make this better... 
        }
    }
    @Override
    public void generateCode(CodeProcedure codeProc) {

    }

}
