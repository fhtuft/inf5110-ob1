package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;


public class RelExp extends Exp {
   
    Exp leftExp,rightExp; 
    RelType rType;

    public RelExp(RelType type,Exp leftExp,Exp rightExp) {
        this.rType = type;
        this.leftExp = leftExp;
        this.rightExp = rightExp;
    }

    public String printAst() {
        return "REL_OP " + rType.toString() + "\n" + "\t" + leftExp.printAst() + "\n" +"\t" + rightExp.printAst();
    }


     public DataType getDType() {
        return new DataType(Type.BOOL);
    }


    @Override
    public Type getType() {
        return Type.BOOL;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        this.leftExp.checkSemantic(symtabl);
        this.rightExp.checkSemantic(symtabl);

        Type leftType = this.leftExp.getType(),rightType = this.rightExp.getType();

        if(!(leftType == Type.INT || leftType == Type.FLOAT))
            throw new SemanticError("Wrong type left exp, is " + leftType.toString());
        if(!(rightType == Type.INT || rightType == Type.FLOAT))
            throw new SemanticError("Wrong type right exp, is " + rightType.toString());


        switch(this.rType) {
            case EQUAL:
                if(leftType == Type.FLOAT || rightType == Type.FLOAT)
                    throw new SemanticError("No float in equal");
            case LESS:
            case LESSEQUAL:
            case GREATER:
            case GREATEREQUAL:
            


        }

   
 }

  @Override
    public void generateCode(CodeProcedure codeProc) {

    }


}
