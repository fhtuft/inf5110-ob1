package syntaxtree;
import java.util.List;

import bytecode.CodeFile;

import compiler.Symtabl;
import compiler.SemanticError;



public class Program extends ASTnode{

    List<Decl> decls;
    String name;

    public Program(String name, List<Decl> decls) {
        System.out.println("Debug Program ");
        this.decls = decls;
        this.name = name;
    }

    public String printAst(){
        StringBuilder sb = new StringBuilder();
        sb.append("(PROGRAM ");
        sb.append("(NAME ");
        sb.append(this.name);
        sb.append(")\n");
        for (Decl decl : decls) {
            sb.append("\t" + decl.printAst());
            sb.append("\n");
        }
        sb.append(")");
        return sb.toString();
        
    }

    @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        for(Decl decl : decls) {
            symtabl.addElement(decl.getName(),decl);
            decl.checkSemantic(symtabl);
        }

        //Check for Main and correct signatur
        Decl main = symtabl.getElement("Main");
        if(main == null)
            throw new SemanticError("Program: No main ");
        
        if(!(main instanceof ProcDecl))
            throw new SemanticError("Program: Main is not at procDecl");
        
        

    }
    

    public void generateCode(CodeFile codefile) {
        System.out.println("generate Code!");
        
    }

}
