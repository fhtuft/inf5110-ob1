package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class AssignStmt extends Stmt {
   
    Var v;  
    Exp e;
 
    public AssignStmt(Var v, Exp e) {
        this.e = e;
        this.v = v;
        
    }
    public String printAst() {
        return "(ASSIGN_STMT " +"\n\t" + v.printAst() + "\n\t" + e.printAst() +")" ;
    }
 @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        this.v.checkSemantic(symtabl);
        this.e.checkSemantic(symtabl);
        Type varType = this.v.getType();
        Type expType = this.e.getType();


        if((varType != expType) && !(varType == Type.FLOAT && expType == Type.INT)  ) {
            throw new SemanticError("AssignStmt: Har differnet types, v: " + varType.toString() + "  exp: " + expType.toString());
        }
 }
    @Override
    public void generateCode(CodeProcedure codeProc) {

    }

}

