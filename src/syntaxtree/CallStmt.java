package syntaxtree;
import java.util.*;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class CallStmt extends Stmt {
    
    String name;
    List<ActParam> al;
   
    Type type;
 
    public CallStmt(String name, List<ActParam> al) {
        this.name = name;
        this.al = al;
    }

    public String printAst() {
        String tmp = "";
        
        for(ActParam item : al) {
            tmp += "\n\t" + item.printAst();
        }

        return "(CALL_STMT " + name + tmp + ")\n";
    }

    
    public Type getType() {
        /* Type is sett in checkSementic, we have not called it before geting type */
        assert this.type != null;

        return this.type;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
        /* Check that that procedure exist in the symbole table */
        Decl tmpDecl = symtabl.getElement(this.name);
        if((tmpDecl == null) || !(tmpDecl instanceof ProcDecl)) {
            throw new SemanticError("Not a procedure: " + this.name);
        }
        ProcDecl procDecl = (ProcDecl)tmpDecl;
        
        //Need this for CallStmtExp 
        this.type = procDecl.getType();

        //Get the formel paramters 
        List<ParamDecl> pList = procDecl.getParamDecl();
        assert pList != null;
        
        //Size for formel is same as acctual 
        if(pList.size() != al.size()) {
                throw new SemanticError("Wrong size of parameter list");
        }
        
        int parameterIndex = 0;
        //Iterarate over actParam and formel paramter 
        Iterator<ParamDecl> iter = pList.iterator();
		for (ActParam actParam : al) {
            ParamDecl pd = iter.next();

            // Semantic check on Parameter 		
			actParam.checkSemantic(symtabl);
            Type actParamType = actParam.getType();
            Type paramType = pd.getType();            

            // Check that type is same on formel paramer and actuall parameter 
			if (actParamType != paramType && !(actParamType == Type.INT && paramType == Type.FLOAT) ) {
				throw new SemanticError("Wrong type on paramer " + parameterIndex + " type is "+paramType.toString() +" Should be " +actParamType.toString());
			}

             parameterIndex++; 
		} 
    
    }

    @Override
    public void generateCode(CodeProcedure codeProc) {

    }

}

