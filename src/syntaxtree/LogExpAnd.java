package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class LogExpAnd extends LogExp{
   
    Exp leftExp,rightExp;
    Type type;
    
 
    public LogExpAnd(Exp leftExp,Exp rightExp) {
        this.leftExp = leftExp;
        this.rightExp = rightExp;
        this.type = Type.BOOL;
    }
    public String printAst() {
        return "(LOG_OP & " + "\n" + "\t" +leftExp.printAst() + "\n" + "\t" + rightExp.printAst() +")\n";
    }

       
   public DataType getDType() {
        return new DataType(Type.BOOL);
    }


    @Override
    public Type getType() {
        return Type.BOOL;
    }    


     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        this.leftExp.checkSemantic(symtabl);
        this.rightExp.checkSemantic(symtabl);

        Type leftType = this.leftExp.getType(),rightType = this.rightExp.getType();

        if(leftType != Type.BOOL ||  rightType != Type.BOOL)
            throw new SemanticError("Non Bool type, left type: " +leftType.toString() + " right type: " + rightType.toString()); 

             
    }
 @Override
    public void generateCode(CodeProcedure codeProc) {

    }


}
