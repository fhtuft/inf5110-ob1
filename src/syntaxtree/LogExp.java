package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public abstract class LogExp extends Exp {
   
    Exp e1,e2;

//    Exp left,right;
 /*
    public LogExp(Exp left,Exp right) {
        this.left = left;
        this.right = right;
        
    }
   */
     public abstract String printAst();

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }


 @Override
    public void generateCode(CodeProcedure codeProc) {

    }
     

}
