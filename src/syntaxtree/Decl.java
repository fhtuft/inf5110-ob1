package syntaxtree;

import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeFile;
import bytecode.CodeProcedure;


public abstract class Decl extends ASTnode {

    String name;
    
    public Decl () {
        
    }

    abstract public String getName();

    public String printAst() {
        return "(CLASS_DECL (NAME " + name + "))";
    }
/*
     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }
*/
    public abstract void insertSymtabl(Symtabl symtabl) throws SemanticError;


    public abstract void generateCode(CodeProcedure codeProc);

    public abstract void generateCode(CodeFile codefile);

}
