package syntaxtree;

import java.util.*;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeFile;
import bytecode.CodeProcedure;
import bytecode.CodeStruct;
import bytecode.type.*;


 
public class ClassDecl extends Decl{

    String name;
    List<VarDecl> list;
   
    Symtabl localSymTabl;
 
    public ClassDecl (String name,List<VarDecl> list) {
        this.name = name;
        this.list = list;
    }

    public String printAst() {
        
        String tmp = "";       
 
        for(VarDecl item : list) {
            tmp += "\n\t" + item.printAst();
        }
        
        return "(CLASS_DECL (NAME " + name + ")" +tmp + "\n\t)";
    }

    public List<VarDecl> getVarList() {
        return this.list;
    }

    public Symtabl getLocalSymTabl() {
        return this.localSymTabl;
    }


    @Override
    public String getName() {
        return this.name;
    }


     @Override
    public void checkSemantic(Symtabl upper) 
                            throws SemanticError {
        Symtabl symtabl = new Symtabl(upper); //TODO: Is this right, somthing more?? What about parent ?
        for( VarDecl var: list) {
            symtabl.addElement(var.getName(),var);
            var.checkSemantic(symtabl);
        }
        this.localSymTabl = symtabl; //Hack? 
    }

    public void insertSymtabl(Symtabl symtbl) throws SemanticError {
        symtbl.addElement(this.name,this);
    }

    public void generateCode(CodeProcedure codeProc) {
        assert false; //No internal classes in the bytecode!
    }

    private CodeType typeToCodeType(Type myType) {
        CodeType myCodeType;
 
        switch(myType) {
            case FLOAT:
                myCodeType = FloatType.TYPE; 
                break;
            case INT:
                myCodeType = IntType.TYPE;
                break;
            case STRING:
                myCodeType = StringType.TYPE;
                break; 
            case BOOL:
                myCodeType = BoolType.TYPE;
                break;
            case NAME:
                myCodeType = null;//Happy compiler
                assert false;
                break;       
            case VOID:
                myCodeType = VoidType.TYPE;
                break;
            default:
                myCodeType = null; //Happy compiler
                assert false;
                break;
        }

        return myCodeType;

    }    


    public void generateCode(CodeFile codefile) {
        codefile.addStruct(this.name);
        
        CodeStruct myCodeStruct = new CodeStruct(this.name);
    
        for(VarDecl var: list) {
            myCodeStruct.addVariable(var.getName(),this.typeToCodeType(var.getType()));
        }
        codefile.updateStruct(myCodeStruct);
    }


}



