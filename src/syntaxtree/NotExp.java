package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.type.*;
import bytecode.instructions.NOT;

public  class NotExp extends Exp {
   
    private Exp e; 
    private Type type;

    DataType dType;

    public NotExp(Exp e) {
        this.e = e;
    }
    public String printAst() {
        return "(NOT_EXP " + e.printAst() +")\n";
    }

     public DataType getDType() {
        return this.dType;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
        /* Check semantic of operand */
        e.checkSemantic(symtabl);
    
        /* Get the type of the operand */
        this.type = e.getType();
        
        if(this.type != Type.BOOL) 
            throw new SemanticError("Not operator applied to non bool type " +type.toString());
         
 }

    @Override
    public Type getType() {
        /* Must have checked semantic of this before call on getType() */
        assert type != null;
        
        return this.type;    
    }

 @Override
    public void generateCode(CodeProcedure codeProc) {
        this.e.generateCode(codeProc);
        codeProc.addInstruction(new NOT());
    }

}
