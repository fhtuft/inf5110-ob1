package syntaxtree;


public enum  RelType {
    EQUAL("="),
    LESS("<"),
    LESSEQUAL("<="),
    LESSGREATER("<>"),
    GREATER(">"),
    GREATEREQUAL(">=");

    String sym;

    RelType(String sym) {
        this.sym = sym;
    }

    public String toString() {
        return sym;
    }

}   
