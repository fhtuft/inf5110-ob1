package syntaxtree;
import java.util.*;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class IfStmt extends Stmt {
    
    String name;
    Exp e;
    List<Stmt> sl;
    /* Else list*/ 
    List<Stmt> esl;
    public IfStmt(Exp e,List<Stmt> sl) {
        this.e = e;        
        this.sl = sl;
        this.esl = null;
    }

    public IfStmt(Exp e,List<Stmt> sl,List<Stmt> esl) {
        this.e = e;        
        this.sl = sl;
        this.esl = esl;    
    }




    public String printAst() {
        String if_tmp = "";
        String else_tmp = "";
        for(Stmt item: sl) {
            if_tmp += "\n\t" + item.printAst();
        }   
        if(esl != null) {
            else_tmp += "\n" + "ELSE_STMT ";
            for(Stmt item: esl) {
                else_tmp += "\n\t" + item.printAst();
            }
        } 

        return "(IF_STMT " + e.printAst() + if_tmp + else_tmp + "\n\t" + ")";
    }


     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
            /* Check that condition is bool*/
            if(e.getType()  != Type.BOOL) {
                throw new SemanticError("If needs bool type, type is " + e.getType().toString());
            }
            /* Check if body*/
            for(Stmt stmt: sl) {
                stmt.checkSemantic(symtabl);
            }
            /* Chek else body if exist*/
            for(Stmt stmt: esl) {
                stmt.checkSemantic(symtabl);
            }

 }

@Override
    public void generateCode(CodeProcedure codeProc) {

    }

}

