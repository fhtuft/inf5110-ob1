package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.instructions.PUSHINT;

import java.lang.Integer;

public class IntLiteral extends Literal {
   
    String i; 
    int number;

    public IntLiteral(String i) {
        this.i = i;
        this.number = Integer.parseInt(i);
    }
    public String printAst() {
        return "(INT_LITERAL " + i + ")\n";
    }

     @Override 
    public DataType getDType() {
        return new DataType(Type.INT);
    } 
    

    @Override
    public Type getType() {
        return Type.INT;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }

 @Override
    public void generateCode(CodeProcedure codeProc) {
            codeProc.addInstruction(new PUSHINT(this.number));

    }


}
