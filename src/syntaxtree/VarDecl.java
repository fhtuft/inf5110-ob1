package syntaxtree;



import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeFile;
import bytecode.CodeProcedure;
import bytecode.type.*;


public class VarDecl extends Decl {
    
    String name;
    DataType dType;
    
    public VarDecl(String name,DataType dType) {
        this.name = name;
        this.dType = dType;
        System.out.println("name: " + this.name+" dType " + dType );
    }

    @Override
    public String getName() {
        return this.name;
    }

    public String printAst() {
        return "(" +"VAR_DECL "+ name +" "+ dType.printAst() +")";
    }

    public Type getType() {
        return this.dType.getType();
    }

    public DataType getDType() {
        return this.dType;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
        
        Type myType = this.dType.getType();
        String typeName = myType.toString();
        String dataTypeName = this.dType.toString(); 

        switch(myType) {
            case FLOAT:
            case INT:
            case STRING:
            case BOOL:
                break;
            case NAME:
                if(!symtabl.contains(dataTypeName)) {
                    throw new SemanticError("VarDecl: Unkown type: " + dataTypeName );
                }
                break;
            default:
                throw new SemanticError("VarDecl: Unkown type " + typeName);
        
        }

         
        
               
 }


    @Override 
    public void insertSymtabl(Symtabl symtabl) throws SemanticError{
        symtabl.addElement(this.name,this);
    } 

    private CodeType typeToCodeType(Type myType) {
        CodeType myCodeType;
 
        switch(myType) {
            case FLOAT:
                myCodeType = FloatType.TYPE; 
                break;
            case INT:
                myCodeType = IntType.TYPE;
                break;
            case STRING:
                myCodeType = StringType.TYPE;
                break; 
            case BOOL:
                myCodeType = BoolType.TYPE;
                break;
            case NAME:
                myCodeType = null;//Happy compiler
                assert false;
                break;       
            case VOID:
                myCodeType = VoidType.TYPE;
                break;
            default:
                myCodeType = null; //Happy compiler
                assert false;
                break;
        }

        return myCodeType;

    }    


    public void generateCode(CodeFile codefile) {
            codefile.addVariable(this.name);
            codefile.updateVariable(this.name,typeToCodeType(dType.getType()));
    }

    public void generateCode(CodeProcedure codeProc) {
        codeProc.addLocalVariable(this.name,typeToCodeType(dType.getType()));
    }

}

