package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public abstract class Exp extends ASTnode  {
   
 
    public Exp() {
        
    }
    public String printAst() {
        return "Exp "+ "\n";
    }
     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }
    abstract public  Type getType();

    abstract public DataType getDType();

    abstract public void generateCode(CodeProcedure codeProc);

}
