package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;


/*ACTUAL_PARAM*/
public class ActParam extends ASTnode  {
   
    Var v;
    Exp e;
 
    Type type;

    public ActParam(Var v) {
        this.v = v;
        this.e = null;
    }
    public ActParam(Exp e) {
        this.e = e;
        this.v = null;
    }
    public String printAst() {
        
        if(v != null) {
            return "(ACT_PARAM " + v.printAst() +")";
        }else {
            return "(ACT_PARAM " + e.printAst() +")";
        }

    }


    public Type getType() {
        return this.type;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
        if(this.e != null) {
            this.e.checkSemantic(symtabl);
            this.type =  this.e.getType();
        }
        else if(this.v != null) {
            this.v.checkSemantic(symtabl);
            this.type =  this.v.getType();
        }

        
    
        
    }

    public void generateCode(CodeProcedure codeProc) {
        //TODO: Is this right ?
        if(this.e != null) {
            this.e.generateCode(codeProc);
        }else {
            this.v.generateCode(codeProc);
        }

    }


}

