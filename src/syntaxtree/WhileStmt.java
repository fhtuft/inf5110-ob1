package syntaxtree;
import java.util.*;

import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class WhileStmt extends Stmt {
    
    String name;
    Exp condExp;
    List<Stmt> sl;
    
    public WhileStmt(Exp condExp, List<Stmt> sl) {
        this.condExp = condExp;
        this.sl = sl;
    }

    public String printAst() {
        String tmp = "";

        for(Stmt item: sl) {
            tmp += "\n\t" + item.printAst();
        }

        return "(WHILE_STMT  " +condExp.printAst()  + tmp +")\n";
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        /* Check condtition */
        this.condExp.checkSemantic(symtabl);

        Type condType = this.condExp.getType();
        if(condType != Type.BOOL) 
            throw new SemanticError("while condition needs bool, type is " + condType.toString());

       for(Stmt stmt: this.sl) {
            stmt.checkSemantic(symtabl);
        }
        
 }

    public void generateCode(CodeProcedure codeProc) {

    }

}

