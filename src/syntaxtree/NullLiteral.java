package syntaxtree;

import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.instructions.PUSHNULL;

public class NullLiteral extends Literal {
   
 
    public NullLiteral() {
        
    }
    public String printAst() {
        return "(NULL_LITERAL " +")\n";
    }

     @Override 
    public DataType getDType() {
        return new DataType(Type.NULL);
    } 



    public Type getType() {
       return Type.NULL; 
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }


    public void generateCode(CodeProcedure codeProc) {
        codeProc.addInstruction(new PUSHNULL());
    }

}
