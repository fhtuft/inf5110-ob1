package syntaxtree;

import java.util.*;

import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeFile;
import bytecode.CodeProcedure;
import bytecode.type.*;
import bytecode.instructions.RETURN;



public class ProcDecl extends Decl {
    
    String name;
    List<ParamDecl> pList;
    DataType type;
    List<Decl> dList;
    List<Stmt> sList;

    public ProcDecl(String name,List<ParamDecl> pList,DataType type,List<Decl> dList,List<Stmt> sList) {
        this.name = name;
        this.pList = pList;
        this.type = type;
        this.dList = dList;
        this.sList = sList;
    }

    public String printAst() {
        String ptmp = "";
        for(ParamDecl item: pList) {
            ptmp += "\n\t" + item.printAst();
        }
        String dtmp = "";
        for(Decl item: dList) {
            dtmp += "\n\t" + item.printAst();
        }
        String stmp = "";
        
        for(Stmt item: sList) {
            stmp += "\n\t" + item.printAst();
        }

        return "("+ "PROC_DECL " +"NAME "+ name  +" "+  type.printAst() + ptmp  +" " + dtmp + stmp + "\n\t" + ")" ;
    }

    public List<ParamDecl> getParamDecl() {
        return this.pList;
    }

    public Type getType() {
        return this.type.getType();
    }
   
    @Override
    public String getName() {
        return this.name;
    }

 
    @Override
    public void checkSemantic(Symtabl upper) 
                            throws SemanticError {

        
        Symtabl symtabl = new Symtabl(upper); //Add prv

        for(ParamDecl paramdecl: pList) {
            symtabl.addElement(paramdecl.getName(),paramdecl);
            paramdecl.checkSemantic(symtabl); 
        }


        for(Decl decl: dList) {
            symtabl.addElement(decl.getName(),decl);
            decl.checkSemantic(symtabl);
        }


        for(Stmt stmt : sList) {

            stmt.checkSemantic(symtabl);//TODO: Add new on
            
            /* If statment is return,check that the type is the same as this */
            if((stmt instanceof ReturnStmt) && !(type.getType()  == ((ReturnStmt)stmt).getReturnType()) )  
                throw new SemanticError("Return type do not match type of return statment");
        
        }

        /* Check first if not void, then that it is not a empty list then if not check that last statment is a return. return type checked before*/
        if( (type.getType() != Type.VOID) &&  (this.sList.size() != 0) && !(this.sList.get(this.sList.size() -1) instanceof ReturnStmt) )
            throw new SemanticError("Non void Procedure last stament is not return");

        if(this.name.compareTo("Main") == 0) {
            if(type.getType() != Type.VOID)
                throw new SemanticError("Main have non void return type, has " + type.getType().toString());

            if(pList.size() != 0)
                throw new SemanticError("Program: Main don not take paramers, " + pList.size() + " given");
        }
        

 }


    @Override 
    public void insertSymtabl(Symtabl symtabl) throws SemanticError {
        symtabl.addElement(this.name,this);
    }
 
  @Override
    public void generateCode(CodeProcedure codeProc) {
        assert false; //Can not have proc inside other proc
    }

    @Override
    public void generateCode(CodeFile codefile) {

        codefile.addProcedure(this.name);

        CodeType myCodeType;
        
        switch(this.type.getType()) {
            case FLOAT:
                myCodeType = FloatType.TYPE; 
                break;
            case INT:
                myCodeType = IntType.TYPE;
                break;
            case STRING:
                myCodeType = StringType.TYPE;
                break; 
            case BOOL:
                myCodeType = BoolType.TYPE;
                break;
            case NAME:
                myCodeType = null;//Happy compiler
                assert false;
                break;       
            case VOID:
                myCodeType = VoidType.TYPE;
                break;
            default:
                myCodeType = null; //Happy compiler
                assert false;
                break;
        }

        CodeProcedure myCodeProc =  new CodeProcedure(this.name,myCodeType,codefile);
        
        /* Make code for the paramters */        
        for(ParamDecl pDecl: pList) {
            pDecl.generateCode(myCodeProc);
        }
        
        /* Make code for the Declarations */
        for(Decl decl: dList) {
                decl.generateCode(myCodeProc);
        }

        /* Make code for Statments */
        for(Stmt stmt: sList) {
            stmt.generateCode(myCodeProc);
        }
        
        /* Void can have return statment but doing it like this is simpler, and  if there is two the last return is ignored by the runtime system */
        if(type.getType() == Type.VOID) 
            myCodeProc.addInstruction(new RETURN());
 
        codefile.updateProcedure(myCodeProc);

    } 

}

