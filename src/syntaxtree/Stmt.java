package syntaxtree;



import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public abstract class Stmt extends ASTnode {
   
 
    public Stmt() {
        
    }
    public abstract String printAst();

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }

    public abstract void generateCode(CodeProcedure codeProc);

}

