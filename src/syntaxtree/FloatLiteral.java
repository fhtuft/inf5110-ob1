package syntaxtree;


import java.lang.Float; 

import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.instructions.PUSHFLOAT;


public class FloatLiteral extends Literal {
   
    String f;
    float number; 


    public FloatLiteral(String f) {
        this.f = f;
        this.number = Float.parseFloat(f);
    }
    public String printAst() {
        return "(FLOAT_LITERAL "+ f +")\n";
    }
       public DataType getDType() {
        return new DataType(Type.FLOAT);
    }


    @Override
    public Type getType() {
        return Type.FLOAT;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }

 @Override
    public void generateCode(CodeProcedure codeProc) {

        codeProc.addInstruction(new PUSHFLOAT(this.number));

    }

}
