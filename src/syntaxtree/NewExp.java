package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.type.*;
import bytecode.instructions.NEW;

public  class NewExp extends Exp {
   
    DataType dt;
 
    public NewExp(DataType dt) {
        this.dt = dt;
    }
    public String printAst() {
        return "(NEW_EXP " +  dt.printAst() +")\n";
    }

       public DataType getDType() {
        return this.dt;
    }
  

    @Override
    public Type getType() {
        return dt.getType(); //This is stupid!
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        Type myType = dt.getType();
        /* New can only be applied to classes*/
        if(myType  != Type.NAME ) {
            throw new SemanticError("NewExp: Type is not a Class, is a " + myType.toString());
        }

        /* Check that the type is decleard */
        if(!symtabl.contains(dt.toString())) {
            throw new SemanticError("NewExp: Type " + myType.toString() + " do not exist");
        }

 }

 @Override
    public void generateCode(CodeProcedure codeProc) {
            final int number = codeProc.structNumber(dt.toString()); 
            codeProc.addInstruction(new NEW(number));
    }

}
