package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class LogExpElse extends LogExp{
   
    Exp leftExp,rightExp; 

    public LogExpElse(Exp leftExp,Exp rightExp) {
        this.leftExp = leftExp;
        this.rightExp = rightExp;
    }
    
    public String printAst() {
        return "(LOG_OP | "+ "\n\t" +leftExp.printAst() + "\n\t" + rightExp.printAst()+")\n";
    }
    
    @Override
    public Type getType() {
        return Type.BOOL;
    }
   
    public DataType getDType() {
        return new DataType(Type.BOOL);
    }

 
    @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

        this.leftExp.checkSemantic(symtabl);
        this.rightExp.checkSemantic(symtabl);

        Type leftType = this.leftExp.getType(),rightType = this.rightExp.getType();

        if(leftType != Type.BOOL ||  rightType != Type.BOOL)
            throw new SemanticError("Non Bool type, left type: " +leftType.toString() + " right type: " + rightType.toString()); 
 
    }

 @Override
    public void generateCode(CodeProcedure codeProc) {

    }

    

}
