package syntaxtree;

import java.util.List;

import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.instructions.GETFIELD;
import bytecode.instructions.LOADGLOBAL;
import bytecode.instructions.LOADLOCAL;


public class Var extends Exp {
    
    String name;
    Exp e;    

    private DataType dType;
    private Type type;

    private boolean classVar;

    public Var(String name) {
        this.name = name;
        this.classVar = false;
    }
    public Var(Exp e,String name) {
        this.e = e;
        this.name = name;
        this.classVar = true;
    }
    
    public DataType getDType() {
        return this.dType;
    }


    public String printAst() {
        if(this.classVar) 
            return "(. NAME "+ name + " " + e.printAst() +")";
        else
            return "(NAME " + name +")";
    }


@Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
        if(this.e == null) {
        Decl dc = symtabl.getElement(this.name);
        if(dc == null)
            throw new SemanticError("Var: Unknown idenfier, " +this.name );
        if(dc instanceof VarDecl) {
            VarDecl vd = (VarDecl)dc;
            this.dType = vd.getDType();
        
        }else if(dc instanceof ParamDecl){ 
            ParamDecl pd = (ParamDecl)dc;
            this.dType = pd.getDType();

        }else {
            throw new SemanticError("Var: Not a varible,  " + this.name + ",is a " + dc.getClass().getName());
            }
                    
        }else {//Is class var

            this.e.checkSemantic(symtabl);
        
            if(e.getType() != Type.NAME)
                throw new SemanticError("Var: Not a class, is a " + e.getType().toString());

            DataType myDType = e.getDType();
            

            ClassDecl myClass = (ClassDecl)symtabl.getElement(myDType.toString());
            List<VarDecl> classVList = myClass.getVarList();

            for(VarDecl var: classVList) {
                if(var.getName().compareTo(this.name) == 0) {
                    this.dType = var.getDType();
                    return; 
                }
            }
    
            throw new SemanticError("Var: Symbol table do not contain variable name " +this.name);
        }
/*
        Decl dc = symtabl.getElement(this.e.getName());
        if(dc == null)
            throw new SemanticError(" Not a element of the class" );
        if(!(dc instanceof ClassDecl))
            throw new SemanticError(" Not a known class");
        
        ClassDecl cd = (ClassDecl)dc;
        Symtabl classSymTabl = cd.getLocalSymTabl();
        if(!classSymTabl.contains(this.name))
            throw new SemanticError(" Not a known element in the class, " + this.name);
  */  
}

    public Type getType() {
        return this.dType.getType(); //TODO: Look at better naming!! 
    }

 @Override
    public void generateCode(CodeProcedure codeProc) {

		if (this.classVar) {
			this.e.generateCode(codeProc);

            final String nameOfStruct = this.e.getDType().toString();
			codeProc.addInstruction( new GETFIELD(codeProc.fieldNumber(nameOfStruct,this.name),codeProc.structNumber(nameOfStruct)));
	
        } else { 
            final int number = codeProc.variableNumber(this.name);
	        if (number == -1) {
			    codeProc.addInstruction( new LOADGLOBAL(codeProc.globalVariableNumber(this.name)));
		    }else {
		        codeProc.addInstruction( new LOADLOCAL(number));	
	        }

		}

    }

}

