package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;


public enum  ArithType {
    ADD("+"),
    SUB("-"),
    MUL("*"),
    DIV("/"),
    EXPO("#");

    String sym;

    ArithType(String sym) {
        this.sym = sym;
    }

    public String toString() {
        return sym;
    }

}   
