package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeFile;
import bytecode.CodeProcedure;
import bytecode.type.*;



public class ParamDecl extends Decl {

    String name;
    DataType type;    
    Boolean isRef;
    public ParamDecl (String name,DataType type,Boolean isRef) {
        this.name = name;
        this.type = type;
        this.isRef = isRef;
    }

    public String printAst() {
        
        return  "(" + ((isRef) ? "PARAM_DECL REF ": "PARAM_DECL ") + name + " " + type.printAst() + ")";
    }


    @Override
    public String getName()  {
        return this.name;
    }
    public DataType getDType() {
        return this.type;
    }

    public Type getType() {
        return this.type.getType(); //...
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }

     @Override 
    public void insertSymtabl(Symtabl symtabl) throws SemanticError{
        symtabl.addElement(this.name,this);
    } 

    private CodeType typeToCodeType(Type myType) {
        CodeType myCodeType;
 
        switch(myType) {
            case FLOAT:
                myCodeType = FloatType.TYPE; 
                break;
            case INT:
                myCodeType = IntType.TYPE;
                break;
            case STRING:
                myCodeType = StringType.TYPE;
                break; 
            case BOOL:
                myCodeType = BoolType.TYPE;
                break;
            case NAME:
                myCodeType = null;//Happy compiler
                assert false;
                break;       
            case VOID:
                myCodeType = VoidType.TYPE;
                break;
            default:
                myCodeType = null; //Happy compiler
                assert false;
                break;
        }

        return myCodeType;

    }    




    @Override
    public void generateCode(CodeProcedure codeProc) {
        codeProc.addParameter(this.name,this.typeToCodeType(this.type.getType()));
    }


    @Override
    public void generateCode(CodeFile codefile) {
        assert false;
    }

}

