package syntaxtree;

import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public class CallStmtExp extends Exp {
    
    CallStmt cs;
    DataType dType;   
 
    public CallStmtExp(CallStmt cs) {
        this.cs = cs;
    }

    public String printAst() {
        return cs.printAst() + "\n";
    }

      public DataType getDType() {
        return this.dType;
    }
 
    @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {

    this.cs.checkSemantic(symtabl);
    }

    @Override
    public Type getType() {
        return cs.getType();        
    }

 @Override
    public void generateCode(CodeProcedure codeProc) {
        this.cs.generateCode(codeProc);

    }


}

