package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;

public abstract class Literal extends Exp {
   
    String name;
 
    public Literal() {
        
    }
    public String printAst() {
        return "var "+ name +"\n";
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }


}
