package syntaxtree;

public enum Type {
        FLOAT("float"),
        INT("int"),
        STRING("string"),
        BOOL("bool"), 
        NAME(""),   
        NULL("null"),
        VOID("void"); /* For proc decl */

    String name;
    Type(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

}
