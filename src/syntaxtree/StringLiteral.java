package syntaxtree;



import compiler.Symtabl;
import compiler.SemanticError;
import bytecode.CodeProcedure;
import bytecode.instructions.PUSHSTRING;

public class StringLiteral extends Literal {
   
    String name;
 
    public StringLiteral(String name) {
        this.name = name;
    }
    public String printAst() {
        return "(STRING_LITERAL "+ name +")\n";
    }
    @Override 
    public DataType getDType() {
        return new DataType(Type.STRING);
    } 

    @Override
    public Type getType() {
        return Type.STRING;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }

 public void generateCode(CodeProcedure codeProc) {
        codeProc.addInstruction(new PUSHSTRING(codeProc.addStringConstant(this.name)));
    }


}
