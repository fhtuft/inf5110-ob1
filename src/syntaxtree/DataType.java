package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;


public class DataType extends ASTnode {


    Type type;
    String name;

    public DataType(Type t) {
        assert t != Type.NAME;
        this.type = t;
    }
    public DataType(String name) {
        this.name = name;
        this.type = Type.NAME;
    }

    

    public String printAst() {
        if(name == null) 
            return "DATA_TYPE :" + " "+type.toString();
        else
            return "DATA_TYPE :" + " "+name;
            

    }
    
    Type getType() {
        return this.type;
    }

    public String toString() {
        return this.name;
    }

     @Override
    public void checkSemantic(Symtabl symtabl) 
                            throws SemanticError {
 }



}
