package syntaxtree;


import compiler.Symtabl;
import compiler.SemanticError;


public abstract class ASTnode  {
   
    String name;
 
    public ASTnode() {
        
    }
    public String printAst() {
        return "var "+ name +"\n";
    }

    public abstract void checkSemantic(Symtabl symtabl)
			throws SemanticError;




}

